Description
-----------
Given the array of data in testData.json, show a vertical scrolling list of cards for each item, matching the provided comps (screen#.png) based on the data.  
Users should be able to logically interact with each card, though what the app does when the user interacts with them is up to you.  
It’s up to you how to allow switching between the different provided test data, but each data item should be a separate experience (don’t show all cards for all data at the same time).

Due to the time limitations for this project, we can't answer questions related to the instructions or implementation.  
If you have any questions, please use your best judgement and make a note of any assumptions you made which you would have otherwise asked about under normal circumstances.  
You won't be penalized or asked to redo the test based on any noted incorrect assumptions.

Deliverable
-----------
Zip up the full Android Studio project, excluding the build folders (to save size), and send the zip file to us.  
This project should be set up to run on any machine, so it should not have any unresolved/local references.  
An APK or AAB file should also be included in the email, either as a separate file or at the root of the zip directory.

Duration
--------
Feel free to time box your implementation to only a few hours, but don’t spend more than a day on this.  
The complete submission should be returned by the end of the same day these project instructions are received.  
Incomplete implementations will still be considered.

Language
--------
Kotlin is preferred, but Java is acceptable.

Acceptable Libraries (optional)
-------------------------------
Any official Android libraries (ex. androidX, support, etc.), Google libraries (ex. Gson, Dagger 2, etc.), or RxJava/RxAndroid may be used.  
Please request approval for other libraries.

Goals
-----
Write clean, logical code
Reproduce views from comps
Create interactive elements which behave as expected for users
Create a complete, polished experience (partial submissions will still be considered)

Bonus (not required!!!)
-----------------------
Create and populate additional screens/interactions, accessible by interacting with the cards
Treat the provided test data like network responses by mocking a network request triggered by user interactions


Assumptions & Programming Notes
-------------------------------
As per the bonus instructions:
1. The repo that provides the data from testData.json is the AccountsRepo.kt.
   This has been setup (and commented out) as if it were providing data via standard REST calls.
   In it's place, a mock has been setup that provides the data from testData.json.
   This is the structure I'd use for a unit test.

These are some things I'd talk to the designer about:
2. The apply image shows a currency of Ksh, there are no images for the other currencies.
   So I used this image for all of the apply screens.
3. The provided Tala Circle image used in the app title bar does not match the comps in color.
4. I didn't bold the apply loan amount, nor did I format it with commas, even though this is certainly possible.
5. The statuses of the accounts were: bronze, silver and gold, but the images provided were
   bronze, blue and gold. So, even though it didn't make sense, I mapped blue to silver status.
6. The image story images do not contain the same breadth of information as in the comp. 
   In other words, they are cut off short.
7. The email and question icons are different sizes which necessitated a clunky approach.
   Similar comments for the big images for approved, apply and paid.