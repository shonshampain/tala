package com.shonshampain.tala.flows.main

import android.os.Bundle
import com.shonshampain.tala.BaseActivity
import com.shonshampain.tala.R

class MainActivity : BaseActivity() {

    override val vm = MainViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}