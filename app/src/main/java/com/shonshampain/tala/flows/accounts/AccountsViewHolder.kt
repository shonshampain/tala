package com.shonshampain.tala.flows.accounts

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.shonshampain.tala.R

class AccountsViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    val accountImage: ImageView? = view.findViewById(R.id.account_image)
    val accountItem: TextView? = view.findViewById(R.id.account_text)
    val accountDetailItem: TextView? = view.findViewById(R.id.account_sub_text)
    val accountButton: TextView? = view.findViewById(R.id.account_button)
    val statusItem: ImageView? = view.findViewById(R.id.status_image)
    val adImage: ImageView? = view.findViewById(R.id.ad_image)
    val linkText: TextView? = view.findViewById(R.id.link_text)
    val linkImage: ImageView? = view.findViewById(R.id.link_image)
    val dueAmount: TextView? = view.findViewById(R.id.due_amount)
}