package com.shonshampain.tala.flows.accounts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.shonshampain.tala.*
import com.shonshampain.tala.model.Account
import com.shonshampain.tala.model.Locale
import com.shonshampain.tala.usecase.AccountsUseCase
import com.shonshampain.tala.usecase.LocaleUseCase
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

sealed class NavEvents : BaseNavEvents() {
    data class UpdateAccounts(val currency: String, val list: List<Account>) : NavEvents()
}

class AccountsViewModel @Inject constructor(
    private val accountsUseCase: AccountsUseCase,
    private val localeUseCase: LocaleUseCase
) : BaseViewModel() {

    private val _navEvents = MutableLiveData<LiveEvent<BaseNavEvents>>()
    override val navEvents: LiveData<LiveEvent<BaseNavEvents>> = _navEvents

    fun refresh() {
        viewModelScope.launch {
            var currentLocale: String? = null
            var currency = ""
            localeUseCase.getLocale().first().process {
                currentLocale = it.symbol
                currency = it.currency
            }
            currentLocale?.let {
                accountsUseCase.getAccounts().first().process {
                    it.filter { acct -> acct.locale == currentLocale }.forEach {
                        Timber.d(">> updated list: $it")
                    }
                    _navEvents.value = LiveEvent(
                        NavEvents.UpdateAccounts(currency, it.filter { acct -> acct.locale == currentLocale })
                    )
                }.catching {
                    _navEvents.value = LiveEvent(
                        BaseNavEvents.Error(it.message ?: "unknown")
                    )
                }
            }
        }
    }
}