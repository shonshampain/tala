package com.shonshampain.tala.flows.accounts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.shonshampain.tala.*
import com.shonshampain.tala.databinding.FragmentAccountsBinding
import com.shonshampain.tala.usecase.LocaleUseCase
import dagger.android.AndroidInjection
import dagger.android.support.AndroidSupportInjection
import timber.log.Timber
import javax.inject.Inject

class AccountsFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var localeUseCase: LocaleUseCase
    private lateinit var vm: AccountsViewModel
    private lateinit var binding: FragmentAccountsBinding
    private val accountsAdapter = AccountsAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        AndroidSupportInjection.inject(this)
        setupViewModel()
        setupDataBinding(inflater, container)
        setupNavigation()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
    }

    override fun onResume() {
        super.onResume()
        vm.refresh()
        activity?.let { activity ->
            activity.findViewById<ImageView>(R.id.tala_logo)?.setOnClickListener {
                (activity as AppCompatActivity).chooseLocale("Choose Locale", true) {
                    localeUseCase.currentLocale = it
                    vm.refresh()
                }
            }
        }
    }
    private fun setupDataBinding(inflater: LayoutInflater, container: ViewGroup?) {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_accounts, container, false
        )
        binding.lifecycleOwner = this
    }

    private fun setupRecyclerView() {
        binding.accountsRecyclerview.apply {
            this.adapter = accountsAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }

    private fun setupNavigation() {
        vm.navEvents.observe(viewLifecycleOwner,
            ActionObserver {
                Timber.d(">> NEW EVENT $it")
                (activity as BaseActivity).handleErrors(it)
                when (it) {
                    is NavEvents.UpdateAccounts -> {
                        accountsAdapter.set(it.currency, it.list)
                    }
                }
            }
        )
    }

    private fun setupViewModel() {
        vm = ViewModelProvider(this, viewModelFactory)[AccountsViewModel::class.java]
    }
}
