package com.shonshampain.tala.flows.accounts

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.RecyclerView
import com.shonshampain.tala.R
import com.shonshampain.tala.copy
import com.shonshampain.tala.model.Account
import com.shonshampain.tala.model.Loan
import java.lang.RuntimeException

enum class AccountItemTypes {
    APPLY, APPROVED, PAID, DUE, STATUS, AD, INVITE, FAQ
}

class AccountsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val differ: AsyncListDiffer<Account> = AsyncListDiffer(this,
        AccountsDiffer()
    )

    fun set(currency: String, list: List<Account>) {
        val workingList = mutableListOf<Account>()

        list.firstOrNull {
            it.loan != null && it.loan.status == "approved"
        }?.let {
            val x = it.copy()
            x.type = AccountItemTypes.APPROVED.ordinal
            x.currency = currency
            workingList.add(x)
        }

        if (workingList.size == 0) {
            list.firstOrNull {
                it.loan != null && it.loan.status == "paid"
            }?.let {
                val x = it.copy()
                x.type = AccountItemTypes.PAID.ordinal
                workingList.add(x)
            }
        }

        if (workingList.size == 0) {
            list.firstOrNull {
                it.loan == null
            }?.let {
                val x = it.copy()
                x.type = AccountItemTypes.APPLY.ordinal
                x.currency = currency
                workingList.add(x)
            }
        }

        list.firstOrNull {
            it.loan != null && it.loan.status == "due"
        }?.let {
            val x = it.copy()
            x.type = AccountItemTypes.DUE.ordinal
            x.currency = currency
            workingList.add(x)
        }

        list.firstOrNull {
            it.loan != null
        }?.let {
            val x = it.copy()
            x.type = AccountItemTypes.STATUS.ordinal
            workingList.add(x)
        }

        val a = Account(list[0].locale, Loan("", "", 0, 0, 0L), 0L, "")
        a.type = AccountItemTypes.AD.ordinal
        workingList.add(a)

        val b = Account(list[0].locale, Loan("", "", 0, 0, 0L), 0L, "")
        b.type = AccountItemTypes.INVITE.ordinal
        workingList.add(b)

        val c = Account(list[0].locale, Loan("", "", 0, 0, 0L), 0L, "")
        c.type = AccountItemTypes.FAQ.ordinal
        workingList.add(c)

        differ.submitList(workingList.copy())
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View = when(viewType) {
            AccountItemTypes.APPROVED.ordinal, AccountItemTypes.PAID.ordinal, AccountItemTypes.APPLY.ordinal -> {
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.account_item, parent, false)
            }
            AccountItemTypes.DUE.ordinal -> {
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.account_due_item, parent, false)
            }
            AccountItemTypes.STATUS.ordinal -> {
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.account_status_item, parent, false)
            }
            AccountItemTypes.AD.ordinal -> {
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.account_ad_item, parent, false)
            }
            AccountItemTypes.INVITE.ordinal -> {
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.account_link_item, parent, false)
            }
            AccountItemTypes.FAQ.ordinal -> {
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.account_faq_item, parent, false)
            }
            else -> throw RuntimeException("Invalid view type $viewType")
        }
        return AccountsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun getItemViewType(position: Int): Int {
        return differ.currentList[position].type
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as AccountsViewHolder
        val context = holder.view.context
        val item = differ.currentList[position]
        when (item.type) {
            AccountItemTypes.PAID.ordinal -> {
                val d = ContextCompat.getDrawable(context, R.drawable.img_loan_status_paidoff)
                holder.accountImage?.setImageDrawable(d)
                holder.accountItem?.text = context.getString(R.string.paid)
                holder.accountDetailItem?.text = context.getString(R.string.paid_details)
                holder.accountButton?.text = context.getString(R.string.apply_now)
            }
            AccountItemTypes.APPLY.ordinal -> {
                val d = ContextCompat.getDrawable(context, R.drawable.img_loan_status_apply)
                holder.accountImage?.setImageDrawable(d)
                holder.accountItem?.text = context.getString(R.string.apply)
                holder.accountDetailItem?.text = context.getString(R.string.apply_details)
                holder.accountButton?.text = context.getString(R.string.apply_now)
            }
            AccountItemTypes.APPROVED.ordinal -> {
                val d = ContextCompat.getDrawable(context, R.drawable.img_loan_status_approved)
                holder.accountImage?.setImageDrawable(d)
                val amount = item.loan?.approved ?: 0
                holder.accountItem?.text = context.getString(R.string.approved)
                holder.accountDetailItem?.text = context.getString(R.string.approved_details,
                    item.currency, amount)
                holder.accountButton?.text = context.getString(R.string.view_loan_offer)
            }
            AccountItemTypes.DUE.ordinal -> {
                val amount = item.loan?.due ?: 0
                holder.dueAmount?.text = context.getString(R.string.amount, item.currency, amount)
            }
            AccountItemTypes.STATUS.ordinal -> {
                when(item.loan?.level) {
                    "bronze" -> {
                        val d = ContextCompat.getDrawable(context, R.drawable.img_bronze_badge_large)
                        holder.statusItem?.setImageDrawable(d)
                    }
                    "silver" -> {
                        val d = ContextCompat.getDrawable(context, R.drawable.img_blue_badge_large)
                        holder.statusItem?.setImageDrawable(d)
                    }
                    "gold" -> {
                        val d = ContextCompat.getDrawable(context, R.drawable.img_gold_badge_large)
                        holder.statusItem?.setImageDrawable(d)
                    }
                }
            }
            AccountItemTypes.AD.ordinal -> {
                val d = when(item.locale) {
                    "ke" -> ContextCompat.getDrawable(context, R.drawable.img_story_ke)
                    "mx" -> ContextCompat.getDrawable(context, R.drawable.img_story_mx)
                    "ph" -> ContextCompat.getDrawable(context, R.drawable.img_story_ph)
                    else -> throw RuntimeException("Illegal locale ${item.locale}")
                }
                holder.adImage?.setImageDrawable(d)
            }
            AccountItemTypes.INVITE.ordinal -> {
                val d = ContextCompat.getDrawable(context, R.drawable.ic_email)
                holder.linkImage?.setImageDrawable(d)
                holder.linkText?.text = context.getString(R.string.invite)
            }
            AccountItemTypes.FAQ.ordinal -> {
                val d = ContextCompat.getDrawable(context, R.drawable.ic_help)
                holder.linkImage?.setImageDrawable(d)
                val lp = holder.linkImage?.layoutParams
                holder.linkImage?.layoutParams = lp
                holder.linkText?.text = context.getString(R.string.faq)
            }
        }
    }
}