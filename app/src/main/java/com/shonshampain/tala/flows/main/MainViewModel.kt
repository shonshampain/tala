package com.shonshampain.tala.flows.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.shonshampain.tala.BaseNavEvents
import com.shonshampain.tala.BaseViewModel
import com.shonshampain.tala.LiveEvent

class MainViewModel : BaseViewModel() {
    sealed class NavEvents : BaseNavEvents() {
    }
    private val _navEvents = MutableLiveData<LiveEvent<BaseNavEvents>>()
    override val navEvents: LiveData<LiveEvent<BaseNavEvents>> = _navEvents
}