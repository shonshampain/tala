package com.shonshampain.tala.flows.accounts

import androidx.recyclerview.widget.DiffUtil
import com.shonshampain.tala.model.Account

class AccountsDiffer : DiffUtil.ItemCallback<Account>() {

    override fun areItemsTheSame(oldItem: Account, newItem: Account): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Account, newItem: Account): Boolean {
        return oldItem == newItem
    }
}