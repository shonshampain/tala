package com.shonshampain.tala

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dagger.android.AndroidInjection
import timber.log.Timber

abstract class BaseActivity : AppCompatActivity() {

    abstract val vm: BaseViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    fun handleErrors(event: BaseNavEvents) {
        when (event) {
            is BaseNavEvents.Error -> {
                Timber.e(event.message)
                errorDialog(
                    getString(R.string.Error),
                    event.message ?: "unknown",
                    getString(R.string.OK)
                )
            }
        }
    }
}