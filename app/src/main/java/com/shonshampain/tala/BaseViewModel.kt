package com.shonshampain.tala

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel

open class BaseNavEvents {
    data class Error(val message: String?) : BaseNavEvents()
}

abstract class BaseViewModel : ViewModel() {

    abstract val navEvents: LiveData<LiveEvent<BaseNavEvents>>
}