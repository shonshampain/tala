package com.shonshampain.tala

import android.content.Context
import android.content.res.Resources.getSystem
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.io.IOException
import java.io.InputStream
import java.lang.Exception
import java.nio.charset.Charset

fun AppCompatActivity.errorDialog(
    title: String,
    message: CharSequence,
    positive: String,
    negative: String? = null,
    onPositive: () -> Unit = {},
    onNegative: () -> Unit = {},
    clickOutside: Boolean = true
) {

    val ad = AlertDialog.Builder(this, 0)
        .setView(R.layout.error_dialog)
        .create()

    ad.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    ad.show()

    ad.findViewById<TextView>(R.id.error_dialog_title)?.text = title
    ad.findViewById<TextView>(R.id.error_dialog_body)?.text = message
    ad.findViewById<TextView>(R.id.error_dialog_positive_button)?.text = positive
    if (negative != null) {
        ad.findViewById<TextView>(R.id.error_dialog_negative_button)?.text = negative
    } else {
        ad.findViewById<TextView>(R.id.error_dialog_negative_button)?.visibility = View.GONE
    }
    ad.findViewById<TextView>(R.id.error_dialog_positive_button)?.setOnClickListener {
        onPositive()
        ad.dismiss()
    }
    ad.findViewById<TextView>(R.id.error_dialog_negative_button)?.setOnClickListener {
        onNegative()
        ad.dismiss()
    }
    if (!clickOutside) {
        ad.setCancelable(false)
    }
}

fun AppCompatActivity.chooseLocale(
    title: String,
    clickOutside: Boolean = true,
    choose: (locale: String) -> Unit
) {

    val ad = AlertDialog.Builder(this, 0)
        .setView(R.layout.locale_dialog)
        .create()

    ad.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    ad.show()

    ad.findViewById<TextView>(R.id.error_dialog_title)?.text = title
    ad.findViewById<TextView>(R.id.locale_ke)?.setOnClickListener {
        choose("ke")
        ad.dismiss()
    }
    ad.findViewById<TextView>(R.id.locale_mx)?.setOnClickListener {
        choose("mx")
        ad.dismiss()
    }
    ad.findViewById<TextView>(R.id.locale_ph)?.setOnClickListener {
        choose("ph")
        ad.dismiss()
    }
    if (!clickOutside) {
        ad.setCancelable(false)
    }
}

suspend inline fun <reified T> emitWithStatus(crossinline value: suspend () -> T): Flow<Result<T>> {
    return flow {
        var response = Result.failure<T>(Exception("uninitialized"))
        @Suppress("LiftReturnOrAssignment")
        try {
            response = Result.success(value())
        } catch (e: Exception) {
            response = Result.failure(e)
        } finally {
            emit(response)
        }
    }
}

fun <T> Result<T>.process(action: (T) -> Unit): Result<T> {
    getOrNull()?.let {
        action(it)
    }
    return this
}

fun <T> Result<T>.catching(action: (Throwable) -> Unit): Result<T> {
    exceptionOrNull()?.let {
        action(it)
    }
    return this
}

fun <T> List<T>.copy(): List<T> {
    val list = mutableListOf<T>()
    list.addAll(this)
    return list
}

fun loadJSONFromAsset(context: Context, filename: String): String? {
    val json: String? = try {
        val `is`: InputStream = context.assets.open(filename)
        val size: Int = `is`.available()
        val buffer = ByteArray(size)
        `is`.read(buffer)
        `is`.close()
        String(buffer, Charset.forName("UTF-8"))
    } catch (ex: IOException) {
        ex.printStackTrace()
        return null
    }
    return json
}