package com.shonshampain.tala

import androidx.lifecycle.Observer

open class LiveEvent<out T>(private val content: T) {
    private var hasBeenHandled = false

    fun handle(handler: (T) -> Unit) {
        if (!hasBeenHandled) {
            hasBeenHandled = true
            handler(content)
        }
    }
}

class ActionObserver<T>(private val handler: (T) -> Unit) : Observer<LiveEvent<T>> {
    override fun onChanged(value: LiveEvent<T>?) {
        value?.handle(handler)
    }
}
