package com.shonshampain.tala.repo

import android.content.Context
import com.google.gson.Gson
import com.shonshampain.tala.emitWithStatus
import com.shonshampain.tala.loadJSONFromAsset
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import com.shonshampain.tala.model.Account


interface AccountsRepo {
    suspend fun getAccounts(): Flow<Result<List<Account>>>
}

// This is how the class would look like if it were configured for a service,
// e.g. setup to use Retrofit.

//class AccountsRepoImpl @Inject constructor(
//    private val accountsService: AccountsService
//) : AccountsRepo {
//
//    override suspend fun getAccounts(): Flow<Result<List<Account>>> {
//        return emitWithStatus {
//            accountsService.getAccounts()
//        }
//    }
//}

class FakeAccountsRepoImpl @Inject constructor(): AccountsRepo {
    @Inject
    lateinit var gson: Gson
    @Inject
    lateinit var context: Context

    override suspend fun getAccounts(): Flow<Result<List<Account>>> {
        return emitWithStatus {
            val json = loadJSONFromAsset(context, "testData.json")
            gson.fromJson(json, Array<Account>::class.java).toList()
        }
    }
}