package com.shonshampain.tala.usecase

import com.shonshampain.tala.emitWithStatus
import com.shonshampain.tala.model.Locale
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

interface LocaleUseCase {
    var currentLocale: String
    suspend fun getLocale(): Flow<Result<Locale>>
}

class LocaleUseCaseImpl @Inject constructor() : LocaleUseCase {

    private val locales: Map<String, Locale>
    override var currentLocale = "ke"
    init {
        val ke = Locale("ke", "KSh", 30000, 10800000)
        val mx = Locale("mx", "$", 10000, -21600000)
        val ph = Locale("ph", "Php", 25000, 28800000)
        locales = mapOf("ke" to ke, "mx" to mx, "ph" to ph)
    }
    override suspend fun getLocale(): Flow<Result<Locale>> {
        return emitWithStatus {
            locales[currentLocale] ?: throw RuntimeException("Invalid locale $currentLocale")
        }
    }
}