package com.shonshampain.tala.usecase

import com.shonshampain.tala.model.Account
import com.shonshampain.tala.repo.AccountsRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

interface AccountsUseCase {
    suspend fun getAccounts(): Flow<Result<List<Account>>>
}

class AccountsUseCaseImpl @Inject constructor(
    private val accountsRepo: AccountsRepo
) : AccountsUseCase {
    override suspend fun getAccounts(): Flow<Result<List<Account>>> {
        return accountsRepo.getAccounts()
    }
}