package com.shonshampain.tala.model

data class Loan(
    val status: String,
    val level: String,
    val due: Int,
    val approved: Int,
    val dueDate: Long
)