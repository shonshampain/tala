package com.shonshampain.tala.model

data class Locale(
    val symbol: String,
    val currency: String,
    val loanlimit: Int,
    val timezone: Long
)