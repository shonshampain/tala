package com.shonshampain.tala.model

data class Account(
    val locale: String,
    val loan: Loan?,
    val timestamp: Long,
    val username: String
) {
    var type: Int = 0
    var currency: String = ""
}