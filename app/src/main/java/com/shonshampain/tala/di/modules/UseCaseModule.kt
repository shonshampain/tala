package com.shonshampain.tala.di.modules

import com.shonshampain.tala.usecase.AccountsUseCase
import com.shonshampain.tala.usecase.AccountsUseCaseImpl
import com.shonshampain.tala.usecase.LocaleUseCase
import com.shonshampain.tala.usecase.LocaleUseCaseImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Suppress("unused")
@Module
abstract class UseCaseModule {

    @Binds
    @Singleton
    abstract fun provideAccountsUseCase(accountsUseCaseImpl: AccountsUseCaseImpl): AccountsUseCase

    @Binds
    @Singleton
    abstract fun provideLocaleUseCase(localeUseCaseImpl: LocaleUseCaseImpl): LocaleUseCase
}