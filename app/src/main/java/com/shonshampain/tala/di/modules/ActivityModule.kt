package com.shonshampain.tala.di.modules

import com.shonshampain.tala.flows.main.MainActivity
import com.shonshampain.tala.di.ActivityScoped
import com.shonshampain.tala.di.viewmodel.ViewModelModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ActivityModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = [FragmentModule::class, ViewModelModule::class])
    abstract fun providesMainActivity(): MainActivity
}