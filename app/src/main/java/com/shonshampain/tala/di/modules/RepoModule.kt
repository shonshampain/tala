package com.shonshampain.tala.di.modules

import com.shonshampain.tala.repo.AccountsRepo
import com.shonshampain.tala.repo.FakeAccountsRepoImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Suppress("unused")
@Module(includes = [NetworkModule::class])
abstract class RepoModule {

    @Binds
    @Singleton
    abstract fun providesAccountsRepo(accountsRepoImpl: FakeAccountsRepoImpl) : AccountsRepo
}