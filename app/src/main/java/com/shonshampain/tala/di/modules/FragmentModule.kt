package com.shonshampain.tala.di.modules

import com.shonshampain.tala.di.FragmentScoped
import com.shonshampain.tala.flows.accounts.AccountsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun provideAccountsFragment(): AccountsFragment

}