package com.shonshampain.tala.di

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import com.shonshampain.tala.TalaApp
import com.shonshampain.tala.di.modules.*
import com.shonshampain.tala.di.viewmodel.ViewModelModule
import com.shonshampain.tala.flows.accounts.AccountsViewModel
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ActivityModule::class,
        AppModule::class,
        FragmentModule::class,
        NetworkModule::class,
        RepoModule::class,
        UseCaseModule::class,
        ViewModelModule::class
    ]
)

interface AppComponent : AndroidInjector<TalaApp> {
    fun inject(target: AccountsViewModel)
    fun inject(gson: Gson)
    fun inject(context: Context)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }
}